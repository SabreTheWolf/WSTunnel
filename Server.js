const WebSocket = require("ws");
const TCP       = require("net");
const UDP       = require("dgram");
const fs        = require("fs");
const http      = require("http");
let prompt    = require("prompt");
let url         = require('url');
const { profile } = require("console");
const promptSync=require("prompt-sync")();
const proxy = require("socks-proxy-agent");
let agent=new proxy.SocksProxyAgent("socks://127.0.0.1:9050");

let socket=null;

function formatBytes(bytes, decimals = 2) {
	if (bytes === 0) return '0 Bytes';

	const k = 1024;
	const dm = decimals < 0 ? 0 : decimals;
	const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

	const i = Math.floor(Math.log(bytes) / Math.log(k));

	return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

let config={
    session_id: "wstunnel",
    session_password: "password",
    session_max_connections: 20,
    onSessionStartup_CreateInvite: true,
    server_application: "Minecraft",
    server_ip: "127.0.0.1",
    server_port: 25565,
    remote_port: 25565,
    protocol: "tcp",
    http_enable: true,
    http_port: 80,
    track_bandwidth: false,
    log_stream_data: false,
    useTorConnection: false,
    banned_ipaddress: []
};
let SESSION_ID=null;
let CLIENTS = {};
let Bandwidth = {
    Downloads: 0,
    Uploads: 0
}
let profile_={
    nickname: "Host",
    acceptPM: true,
    status: "online",
    bio: ""
}

if(fs.existsSync("server-profile.json")) {
    profile_=JSON.parse(fs.readFileSync("server-profile.json"));
} else {
    profile_["nickname"]=promptSync("Enter your nickname: ");
    profile_["bio"]=promptSync("Enter your bio or if not just leave it empty: ");
    fs.writeFileSync("server-profile.json", JSON.stringify(profile_, null, '\t'));
}

if(fs.existsSync("server-config.json")) {
    config=JSON.parse(fs.readFileSync("server-config.json"));
} else {
    config["session_id"]=promptSync("Enter your own custom session id: ");
    config["session_password"]=promptSync("Enter your session's password: ");
    config["session_max_connections"]=parseInt(promptSync("Enter the max clients your server is accepting: "));
    // if(config["session_id"]=="") {config["session_host_killserver"]=true;}
    config["onSessionStartup_CreateInvite"]=parseInt(promptSync("Enable whitelist and create invite code on start up (0-1): "));
    config["server_application"]=promptSync("Enter the application your using/playing on: ");
    config["server_ip"]=promptSync("Enter the server's ip (127.0.0.1): ");
    config["server_port"]=parseInt(promptSync("Enter the server's port ("+config["server_ip"]+":PORT NUM): "));
    config["remote_port"]=parseInt(promptSync("What port the client will use to connect to: "));

    console.log("Search up if the applicaton support tcp = TCP or udp = UDP.");
    config["protocol"]=promptSync("Enter which protocol the application is using (tcp,udp): ");
    config["http_enable"]=parseInt(promptSync("Enable HTTP Webserver (0-1): "));
    config["http_port"]=parseInt(promptSync("Enter the HTTP port to access the admin panel: "));
    config["track_bandwidth"]=parseInt(promptSync("Enable bandwidth tracking (0-1): "));
    config["log_stream_data"]=parseInt(promptSync("Enable logging stream data (0-1): "));

    fs.writeFileSync("server-config.json", JSON.stringify(config, null, '\t'));
}

var schema = {
    properties: {
        Chat: {
            name: '',
            description: '',
            message: ''
        }
    }
}

prompt.message='';
prompt.delimiter='';

var promptChat = () => {
    prompt.get(schema, function(err, result) {
        var cmd=result.Chat.split(" ");
        switch(cmd[0]) {
            case "/help":
                console.log("=== Common Commands ===");
                console.log("/help - Displays the list of commands.");
                console.log("");
                console.log("=== Moderation Commands ===")
                console.log("/kick [socket id] [reason] - Kicks the socket id with the reason.");
                console.log("/ban-ip [ip] [duration] [socket id (optional)] [reason] - Bans the ip address with the optional socket id.");
                console.log("/unban-ip [ip] - Unbans the ip address to the session.");
                console.log("");
                console.log("=== Player Reporting ===");
                console.log("/greport [socket id] [reason for report] - Sends a global report to the Instantiated Staff team. (Note: When sending a report please do not tell the user you are reporting them. They can leave the session which will make reporting them useless.)");
                console.log("");
                console.log("=== Profile Commands ===");
                console.log("/edit-nickname [alias] - Edits the user's nickname.");
                console.log("/toggle-pm - Toggles the ability to let you see private messages.");
                console.log("/bio [your bio here] - Sets the profile's bio for others to read.");
                console.log("/status [online,away,custom <write your own status>]")
                console.log("/check-status [socket id] - Sends the socket id a message and the socket will reply back with the status.");
                console.log("/read-bio [socket id] - Reads the user's bio.");
                console.log("");
                console.log("=== Chat Commands ===");
                console.log("/pm [socket id] [message] - Sends a message to the socket if they have private messages enabled.");
                console.log("[message] - Sends chat messages to everyone on the session.");
                break;
            case "/greport":
                var socket_id=parseInt(cmd[1]);
                var reason="";
                for(var i=2; i<cmd.length; i++) {
                    if(i==2) {reason+=cmd[i];}else{reason+=" "+cmd[i];}
                }
                var globalreport=JSON.stringify({
                    msg: "FileReport",
                    socketid: socket_id,
                    Message: reason
                });
                socket.send(globalreport);
                break;
            case "/kick":
                var socket_id=parseInt(cmd[1]);
                var kickreason="";
                for(var i=2; i<cmd.length; i++) {
                    if(i==2) {kickreason+=cmd[i];}else{kickreason+=" "+cmd[i];}
                }
                var kick=JSON.stringify({
                    msg: "kick",
                    instance_guid: SESSION_ID,
                    socketid: socket_id,
                    reason: kickreason
                });
                socket.send(kick);
                break;
            case "/ban-ip":
                var ip=cmd[1];
                var dur=parseInt(cmd[2]);
                var banreason="";
                for(var i=4; i<cmd.length; i++) {
                    if(i==4) {banreason+=cmd[i];}else{banreason+=" "+cmd[i];}
                }
                var socket_id=parseInt(cmd[3]);

                var ban=JSON.stringify({
                    msg: "ban_ip",
                    instance_guid: SESSION_ID,
                    banip: ip,
                    duration: dur,
                    reason: banreason
                });
                socket.send(ban);
                break;
            case "/unban-ip":
                var ip=cmd[1];

                var unban=JSON.stringify({
                    msg: "unban_ip",
                    instance_guid: SESSION_ID,
                    unbanip: ip
                });
                socket.send(unban);
                break;
            case "/edit-nickname":
                var nickchange="";
                for(var i=1; i<cmd.length; i++) {
                    if(i==1) {nickchange+=cmd[i];}else{nickchange+=" "+cmd[i];}
                }
                profile_["nickname"] = nickchange;
                fs.writeFileSync("server-profile.json", JSON.stringify(profile_, null, '\t'));
                break;
            case "/toggle-pm":
                if(profile_["acceptPM"]==true) {profile_["acceptPM"]=false;}else{profile_["acceptPM"]=true;}
                fs.writeFileSync("server-profile.json", JSON.stringify(profile_, null, '\t'));
                break;
            case "/bio":
                var bio="";
                for(var i=1; i<cmd.length; i++) {
                    if(i==1){bio+=cmd[i];}else{bio+=" "+cmd[i];}
                }
                profile_["bio"]=bio;
                fs.writeFileSync("server-profile.json", JSON.stringify(profile_, null, '\t'));
                break;
            case "/status":
                var s="";
                for(var i=1; i<cmd.length; i++) {
                    if(i==1) {s+=cmd[i];}else{s+=" "+cmd[i];}
                }
                profile_["status"]=s;
                fs.writeFileSync("server-profile.json", JSON.stringify(profile_, null, '\t'));
                break;
            case "/check-status":
                var socket_id=parseInt(cmd[1]);

                var checkStatus=JSON.stringify({
                    msg: "socket_to",
                    DataStream: {
                        id: "check_status"
                    },
                    SocketID: socket_id
                });
                socket.send(checkStatus);
                break;
            case "/read-bio":
                var socket_id=parseInt(cmd[1]);

                var readBio=JSON.stringify({
                    msg: "socket_to",
                    DataStream: {
                        id: "read_bio"
                    },
                    SocketID: socket_id
                });
                socket.send(readBio);
                break;
            case "/pm":
                var socket_id=parseInt(cmd[1]);
                var chatmsg="";
                for(var i=2; i<cmd.length; i++) {
                    if(i==2) {chatmsg+=cmd[i];}else{chatmsg+=" "+cmd[i];}
                }

                var pm=JSON.stringify({
                    msg: "socket_to",
                    DataStream: {
                        id: "pm_chat",
                        nickname: profile_["nickname"],
                        chat_message: chatmsg
                    },
                    SocketID: socket_id
                });
                socket.send(pm);
                console.log(profile_["nickname"]+" >> "+chatmsg);
                break;
            default:
                if(result.Chat != "") {
                    var send_chat=JSON.stringify({
                        msg: "sent",
                        instance_guid: SESSION_ID,
                        DataStream: {
                            id: "client_chat",
                            chatmsg: result.Chat,
                            nick: profile_["nickname"]
                        }
                    });
                    socket.send(send_chat);
                }
                break;
        }
        setTimeout(function() {promptChat();}, 1500);
    });
}

if(config["useTorConnection"]) {
    socket = new WebSocket("ws://servermckvakd3l5aswm2uklnvl4iahcvov7te3l66loj7rsnfvbdvqd.onion:12701", { agent: agent });
} else {
    socket = new WebSocket("wss://instantiated.xyz:12701");
}
socket.on("open", () => {
    setInterval(function() {
        if(socket.readyState == socket.OPEN) {
            socket.send(`{"msg":"ping"}`);
        }
    },30000);

    console.log("Chat Initialized! Remember to type /help for a list of commands.");
    if(config["session_id"] == null || config["session_id"] == "") {
        var createSession=JSON.stringify({
            msg: "create_server_session",
            Title: "Websocket Reverse Tunnel",
            max_clients: config["session_max_connection"],
            whitelist: config["onSessionStartup_CreateInvite"],
            passkey: config["session_password"],
            host_killserver: 1
        });
        socket.send(createSession);
        promptChat();
    } else {
        var createServer=JSON.stringify({
            msg: "server_only",
            SessionID: config["session_id"],
            Title: "Websocket Reverse Tunnel",
            max_clients: config["session_max_connection"],
            whitelist: config["onSessionStartup_CreateInvite"],
            passkey: config["session_password"]
        });
        socket.send(createServer);
        promptChat();
    }
});

socket.on("close", () => {
    console.log("The connection to the server was lost.");
    process.exit();
});

socket.on("error", () => {
    console.log("An error has occured.");
    process.exit();
});

socket.on("message", (msg) => {
    var id=JSON.parse(msg);

    switch(id["MSG"]) {
        case "filed_success":
            console.log("Successfully submitted your report to the staff team!");
            break;
        case "file_submit_failed":
            console.log("Report failed to submit:");
            console.log("- The socket id you provided is no longer available or around.");
            console.log("- The message was blank. Please enter a message before you report the user.");
            break;
        case "filed_report":
            console.log("Report received!");
            console.log("By - socket #"+id["By"]);
            console.log("Against - socket #"+id["Against"]);
            console.log("Reason\n"+id["Reason"]);
            console.log("");
            console.log("Reporter's IP Address ["+id["IP"]+"]");
            break;
        case "server_already_exists":
            console.log("Server already exists...");
            break;
        case "SocketID":
            console.log("Connection was successful! Your ID is "+id["SocketID"]);
            break;
        case "created_session":
            SESSION_ID=id["SessionGUID"];
            console.log("Session created successfully! ID: "+SESSION_ID);
            for(var i=0; i<config["banned_ipaddress"].length; i++) {
                var sendBanIP=JSON.stringify({
                    msg: "ban_ip",
                    instance_guid: SESSION_ID,
                    banip: config["banned_ipaddress"][i]["ip"],
                    socketid: null,
                    reason: config["banned_ipaddress"][i]["reason"],
                    duration: null
                });
                socket.send(sendBanIP);
            }
            if(config["onSessionStartup_CreateInvite"]==true) {
                var createInvite=JSON.stringify({
                    msg: "CreateInvite",
                    instance_guid: SESSION_ID,
                    limitUse: 100
                });
                socket.send(createInvite);
            }
            break;
        case "Invite_Created":
            console.log("Invite code: "+id["Code"]+"\nLimit Use: "+id["Limits"]);
            break;
        case "joined_session":
            console.log("Client "+id["SocketID"]+" has joined the session. IP Address {"+id["IPAddress"]+"}");
            fs.appendFileSync("iplogs.txt", "Client #"+id["SocketID"]+" has connected, ipaddr="+id["IPAddress"]+"\r\n");
            if(config["protocol"] == "tcp") {
                CLIENTS["client_"+id["SocketID"]] = {
                    socket_id: id["SocketID"],
                    client_conn: [],
                    IpAddr: id["IPAddress"],
                    Flags: id["Flags"],
                    Bandwidth: {
                        Upload: 0,
                        Download: 0
                    },
                    canAcceptPackets: true
                };
            } else {
                CLIENTS["client_"+id["SocketID"]] = {
                    socket_id: id["SocketID"],
                    client_conn: new UDP.createSocket("udp4"),
                    IpAddr: id["IPAddress"],
                    Flags: id["Flags"],
                    Bandwidth: {
                        Upload: 0,
                        Download: 0
                    },
                    canAcceptPackets: true
                };
                CLIENTS["client_"+id["SocketID"]]["client_conn"].on("message", (msg) => {
                    if(config["track_bandwidth"]==true) {CLIENTS["client_"+id["SocketID"]]["Bandwidth"]["Download"]+=msg.length; Bandwidth["Downloads"]+=msg.length;}
                    var hostSendBack=JSON.stringify({
                        msg: "private_msg",
                        instance_guid: SESSION_ID,
                        DataStream: {
                            id: "server_sent",
                            dataStream: msg
                        },
                        socket: id["SocketID"]
                    });
                    socket.send(hostSendBack);
                });
                CLIENTS["client_"+id["SocketID"]]["client_conn"].on("error", () => {
                    console.log("There was a problem with the socket.");
                })
            }
            var sendConn=JSON.stringify({
                msg: "private_msg",
                instance_guid: SESSION_ID,
                DataStream: {
                    id: "create_connection",
                    ip: config["server_ip"],
                    port: config["remote_port"],
                    protocol: config["protocol"],
                    application: config["server_application"],
                    is_logging: config["log_stream_data"]
                },
                socket: id["SocketID"]
            });
            socket.send(sendConn);
            break;
        case "Tx":
            var tx=id["DataStream"];

            switch(tx["id"]) {
                case "client_chat":
                    console.log("<"+tx["nick"]+" from socket "+id["SocketID"]+">: "+tx["chatmsg"]);

                    var send_chat=JSON.stringify({
                        msg: "received",
                        instance_guid: SESSION_ID,
                        DataStream: {
                            id: "client_chat",
                            chatmsg: tx["chatmsg"],
                            nick: tx["nick"],
                            socket_id: id["SocketID"]
                        }
                    });
                    socket.send(send_chat);
                    break;
                case "client_sent":
                    if(config["protocol"] == "tcp") {
                        if(CLIENTS["client_"+id["SocketID"]]["canAcceptPackets"]) {
                            CLIENTS["client_"+id["SocketID"]]["client_conn"][tx["pos"]].write(Buffer.from(tx["dataStream"]));
                        }
                        if(config["log_stream_data"]) {fs.appendFileSync("client"+id["SocketID"]+".txt", Buffer.from(tx["dataStream"])+"\r\n");}
                        if(config["track_bandwidth"]==true) {CLIENTS["client_"+id["SocketID"]]["Bandwidth"]["Upload"]+=Buffer.from(tx["dataStream"]).length; Bandwidth["Uploads"]+=Buffer.from(tx["dataStream"]).length;}
                    } else {
                        if(CLIENTS["client_"+id["SocketID"]]["canAcceptPackets"]) {
                            CLIENTS["client_"+id["SocketID"]]["client_conn"].send(Buffer.from(tx["dataStream"]), config["server_port"], config["server_ip"]);
                        }
                        if(config["log_stream_data"]) {fs.appendFileSync("client"+id["SocketID"]+".txt", Buffer.from(tx["dataStream"])+"\r\n");}
                        if(config["track_bandwidth"]==true) {CLIENTS["client_"+id["SocketID"]]["Bandwidth"]["Upload"]+=Buffer.from(tx["dataStream"]).length; Bandwidth["Uploads"]+=Buffer.from(tx["dataStream"]).length;}
                    }
                    break;
                case "client_connect":
                    console.log("Client is connecting...");
                    var idCli=CLIENTS["client_"+id["SocketID"]]["client_conn"].length;
                    CLIENTS["client_"+id["SocketID"]]["client_conn"].push(new TCP.Socket());
                    // Connection to the server. Well at least try to anyways.
                    // CLIENTS["client_"+id["SocketID"]]["client_conn"].connect(config["server_port"], config["server_ip"], function(){});
                    CLIENTS["client_"+id["SocketID"]]["client_conn"][idCli].on("data", (data) => {
                        if(config["track_bandwidth"]==true) {CLIENTS["client_"+id["SocketID"]]["Bandwidth"]["Download"]+=data.length; Bandwidth["Downloads"]+=data.length;}
                        var hostSendBack=JSON.stringify({
                            msg: "private_msg",
                            instance_guid: SESSION_ID,
                            DataStream: {
                                id: "server_sent",
                                dataStream: data,
                                pos: idCli
                            },
                            socket: id["SocketID"]
                        });
                        socket.send(hostSendBack);
                    });
                    CLIENTS["client_"+id["SocketID"]]["client_conn"][idCli].on("error", ()=>{
                        console.log("There was a problem with the socket.");
                        // CLIENTS["client_"+id["SocketID"]]["client_conn"].connect(config["server_port"], config["server_ip"], function(){});
                    });
                    CLIENTS["client_"+id["SocketID"]]["client_conn"][idCli].on("close", ()=>{
                        /* setTimeout(function() {
                            CLIENTS["client_"+id["SocketID"]]["client_conn"].connect(config["server_port"],config["server_ip"],function(){});
                        }, 1000); */
                        //console.log(CLIENTS["client_"+id["SocketID"]]);
                    });
                    CLIENTS["client_"+id["SocketID"]]["client_conn"][idCli].connect(config["server_port"], config["server_ip"], function(){});
                    break;
                case "client_disconnect":
                    console.log("Client is disconnecting...");
                    CLIENTS["client_"+id["SocketID"]]["client_conn"][tx["pos"]].end();
                    CLIENTS["client_"+id["SocketID"]]["client_conn"][tx["pos"]].removeAllListeners("data");
                    CLIENTS["client_"+id["SocketID"]]["client_conn"][tx["pos"]].removeAllListeners("close");
                    CLIENTS["client_"+id["SocketID"]]["client_conn"][tx["pos"]].removeAllListeners("error");
                    CLIENTS["client_"+id["SocketID"]]["client_conn"].splice(tx["pos"], 1);
                    break;
            }
            break;
        case "SocketRx":
            var s=id["DataStream"];

            switch(s["id"]) {
                case "pm_chat":
                    if(profile_["acceptPM"] == true) {
                        console.log(s["nickname"]+" from socket "+id["From"]+" << "+s["chat_message"]);
                    }
                    break;
                case "check_status":
                    var backCheckStatus=JSON.stringify({
                        msg: "socket_to",
                        DataStream: {
                            id: "got_status",
                            nickname: profile_["nickname"],
                            status: profile_["status"]
                        },
                        SocketID: id["From"]
                    });
                    socket.send(backCheckStatus);
                    break;
                case "got_status":
                    switch(s["status"]) {
                        case "online":
                            console.log(s["nickname"]+" is Online.");
                            break;
                        case "away":
                            console.log(s["nickname"]+" is Away From Keyboard.");
                            break;
                        default:
                            console.log(s["nickname"]+" is "+s["status"]);
                            break;
                    }
                    break;
                case "read_bio":
                    var backBioRead=JSON.stringify({
                        msg: "socket_to",
                        DataStream: {
                            id: "got_bio",
                            nickname: profile_["nickname"],
                            bio: profile_["bio"]
                        },
                        SocketID: id["From"]
                    });
                    socket.send(backBioRead);
                    break;
                case "got_bio":
                    console.log("["+s["nickname"]+"]\n\n"+s["bio"]);
                    break;
            }
            break;
        case "Disconnected":
            console.log("Client "+id["SocketID"]+" has lefted the session.");
            if(config["protocol"] == "tcp") {
                for(var ilk=0; ilk<CLIENTS["client_"+id["SocketID"]]["client_conn"].length; ilk++) {
                    CLIENTS["client_"+id["SocketID"]]["client_conn"][ilk].end();
                    CLIENTS["client_"+id["SocketID"]]["client_conn"][ilk].removeAllListeners("data");
                    CLIENTS["client_"+id["SocketID"]]["client_conn"][ilk].removeAllListeners("close");
                    CLIENTS["client_"+id["SocketID"]]["client_conn"][ilk].removeAllListeners("error");
                }
            } else {
                CLIENTS["client_"+id["SocketID"]]["client_conn"].close();
                CLIENTS["client_"+id["SocketID"]]["client_conn"].removeAllListeners("message");
            }
            delete CLIENTS["client_"+id["SocketID"]];
            break;
    }
});

if(config["http_enable"] == true) {
    console.log("Webserver Moderation listening at http://127.0.0.1:"+config["http_port"]+"/");
    http.createServer((req, res) => {
        res.writeHead(200, {'Content-Type': 'text/html'});
        var q = url.parse(req.url, true).query;
        switch(q.page) {
            case "ban":
                var banIP=JSON.stringify({
                    msg: "ban_ip",
                    instance_guid: SESSION_ID,
                    banip: q["ip"],
                    socketid: parseInt(q["socket"]),
                    reason: q["reason"],
                    duration: parseInt(q["duration"])
                });
                if(q["duration"]=="permanent") {
                    banIP["duration"] = null;
                    config["banned_ipaddress"].push({
                        ip: q["ip"],
                        reason: q["reason"]
                    });
                    fs.writeFileSync("server-config.json", JSON.stringify(config, null, '\t'));
                }
                socket.send(banIP);
                res.write(`<html><head></head><body>Attempted IP Address Ban Sent!<br><a href="/">Go back to main page</a></body></html>`);
                break;
            case "unban":
                var unbanIP=JSON.stringify({
                    msg: "unban_ip",
                    instance_guid: SESSION_ID,
                    unbanip: q["ip"]
                });
                socket.send(unbanIP);
                for(var i=0; i<config["banned_ipaddress"].length; i++) {
                    if(config["banned_ipaddress"][i]["ip"] == q["ip"]) {
                        config["banned_ipaddress"].splice(i, 1);
                        fs.writeFileSync("server-config.json", JSON.stringify(config, null, '\t'));
                        break;
                    }
                }
                res.write(`<html><head></head><body>Attempted IP Address Unban Sent!<br><a href="/">Go back to main page</a></body></html>`);
                break;
            case "kick":
                var kickSocket=JSON.stringify({
                    msg: "kick",
                    instance_guid: SESSION_ID,
                    socketid: parseInt(q["socket"]),
                    reason: q["reason"]
                });
                socket.send(kickSocket);
                res.write(`<html><head></head><body>Attempted Kicking SocketID `+q["socket"]+` Sent!<br><a href="/">Go back to main page</a></body></html>`);
                break;
            case "get_status":
                res.writeHead(200, {"Content-Type": "application/json"});
                res.write(JSON.stringify({ClientList: CLIENTS, ServerBandwidth: Bandwidth}));
                break;
            case "lock":
                if(CLIENTS[q["id"]]["canAcceptPackets"]) {
                    CLIENTS[q["id"]]["canAcceptPackets"]=false;
                } else {
                    CLIENTS[q["id"]]["canAcceptPackets"]=true;
                }
                res.write(`<html><head></head><body>Attempted to lock the client's connection!<br><a href="/">Go back to main page</a></body></html>`);
                break;
            default:
                var html=`<html>
                <head>
                    <title>Websocket Tunnel Administration Panel</title>
                </head>
                <body>
                    <p id="serverbandwidth">Server Bandwidth:<br>
                    <i>`+formatBytes(Bandwidth["Uploads"])+` Uploaded</i><br>
                    <i>`+formatBytes(Bandwidth["Downloads"])+` Downloaded</i></p>
                    <p>Client List</p>
                    <ul id="clients">`;
                for(clients in CLIENTS) {
                    html+=`<li>`+clients+`<br>`+CLIENTS[clients]["IpAddr"]+`, `+formatBytes(CLIENTS[clients]["Bandwidth"]["Upload"])+` Uploaded | `+formatBytes(CLIENTS[clients]["Bandwidth"]["Download"])+` Downloaded. [Accepts Packets: `+CLIENTS[clients]["canAcceptPackets"]+`] <button onclick="toggleAcceptPackets('`+clients+`')">Toggle Accepting Packets</button><li>`;
                }
                html+=`</ul><h1>Moderation</h1>
                    <p>Ban IP Address: <input type="text" id="ipBan" placeholder="192.168.1.123" />, Socket: <input type="number" id="socketId_ban" />, Reason: <input type="text" id="banReason" />, Duration: <select id="banDuration"><option value="permanent">Permanent Ban</option><option value="3600">1 Hour</option><option value="86400">1 Day</option><option value="604800">1 Week</option><option value="2592000">1 Month</option><option value="31104000">1 Year</option></select> <button onclick="banIP();">Ban IP Address</button></p>
                    <p>Unban IP Address: <input type="text" id="ipUnban" placeholder="192.168.1.123" /> <button onclick="unbanIP();">Unban IP Address</button></p>
                    <p>Kick Socket: <input type="number" id="socketId_kick" /> Reason: <input type="text" id="socketId_reason" placeholder="Kick reason." /> <button onclick="kickSocket();">Kick Client</button></p>
                    <script>
                        function toggleAcceptPackets(id) {
                            if(confirm("Do you really wanna freeze this client's connections? They will be timed out or never receive any packets.")) {
                                // window.location.href="/?page=lock&id="+id;
                                var HTTPLock=new XMLHttpRequest();
                                HTTPLock.open("GET", "/?page=lock&id="+id, true);
                                HTTPLock.send();
                            }
                        }
                        function banIP() {
                            window.location.href="/?page=ban&ip="+document.getElementById("ipBan").value+"&socket="+document.getElementById("socketId_ban").value+"&reason="+document.getElementById("banReason").value+"&duration="+document.getElementById("banDuration").value;
                        }
                        function unbanIP() {
                            window.location.href="/?page=unban&ip="+document.getElementById("ipUnban").value;
                        }
                        function kickSocket() {
                            window.location.href="/?page=kick&socket="+document.getElementById("socketId_kick").value+"&reason="+document.getElementById("socketId_reason").value;
                        }
                        function formatBytes(bytes, decimals = 2) {
                            if (bytes === 0) return '0 Bytes';
                        
                            const k = 1024;
                            const dm = decimals < 0 ? 0 : decimals;
                            const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                        
                            const i = Math.floor(Math.log(bytes) / Math.log(k));
                        
                            return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
                        }
                        function getStatus(callback) {
                            var HTTPStatus=new XMLHttpRequest();
                            HTTPStatus.open("GET", "/?page=get_status", true);
                            HTTPStatus.onload=function() {
                                callback(this.responseText);
                            }
                            HTTPStatus.send();
                        }
                        setInterval(() => {
                            getStatus((data) => {
                                var CLIENTS=JSON.parse(data);
                                var html="";
                                for(clients in CLIENTS["ClientList"]) {
                                    html+="<li>"+clients+"<br>"+CLIENTS["ClientList"][clients]["IpAddr"]+", "+formatBytes(CLIENTS["ClientList"][clients]["Bandwidth"]["Upload"])+" Uploaded | "+formatBytes(CLIENTS["ClientList"][clients]["Bandwidth"]["Download"])+" Downloaded. [Accepts Packets: "+CLIENTS["ClientList"][clients]["canAcceptPackets"]+"] <button onclick=\\"toggleAcceptPackets('"+clients+"')\\">Toggle Accepting Packets</button><li>";
                                }
                                document.getElementById("clients").innerHTML = html;
                                document.getElementById("serverbandwidth").innerHTML="Server Bandwidth:<br><i>"+formatBytes(CLIENTS["ServerBandwidth"]["Uploads"])+" Uploaded</i><br><i>"+formatBytes(CLIENTS["ServerBandwidth"]["Downloads"])+" Downloaded</i>";
                            })
                        }, 1500);
                    </script>
                </body>
            </html>`;
                // TODO: BAN
                res.write(html);
                break;
        }
        res.end();
    }).listen(config["http_port"]);
}