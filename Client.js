const WebSocket = require("ws");
const TCP       = require("net");
const UDP       = require("dgram");
const fs        = require("fs");
let prompt    = require("prompt");
const proxy = require("socks-proxy-agent");
let agent=new proxy.SocksProxyAgent("socks://127.0.0.1:9050");

let socket=null;

function convertEpochToDate(seconds) {
    var utcSeconds = seconds;
    var d = new Date(0);
    d.setUTCSeconds(utcSeconds);
    return d;
}

function convertEpochToCountdown(number) {
    // 0yrs 0mths 0days 0hrs 0mins 0secs
    var years=Math.floor(number/31557600)*2099;
    var months=Math.floor(number/2629800)*12;
    var days=Math.floor(number/86400)*365;
    var hours=Math.floor(number/3600)*60;
    var minutes=Math.floor(number/60)*60;
    var seconds=Math.floor(number/60)*60;

    return years+"yrs "+months+"mths "+days+"days "+hours+"hrs "+minutes+"mins "+seconds+"secs";
}

var config={
    session_id: "wstunnel",
    session_password: "password",
    invite_code: "",
    listen_ipaddress: "127.0.0.1",
    useTorConnection: false
};
var SESSION_ID=null;
var Server=null;
var ServerUDP=null;
var Protocol=null;
var Connection=null;
var PreviousConnection=null;
var Port=null;
var Sockets=[];
let profile_={
    nickname: "anonymous",
    acceptPM: true,
    status: "online",
    bio: ""
}

if(fs.existsSync("client-profile.json")) {
    profile_=JSON.parse(fs.readFileSync("client-profile.json"));
} else {
    fs.writeFileSync("client-profile.json", JSON.stringify(profile_, null, '\t'));
}

if(fs.existsSync("client-config.json")) {
    config=JSON.parse(fs.readFileSync("client-config.json"));
} else {
    fs.writeFileSync("client-config.json", JSON.stringify(config, null, '\t'));
}

var schema = {
    properties: {
        Chat: {
            message: ''
        }
    }
}

prompt.message='';
prompt.delimiter='';

var promptChat = () => {
    prompt.get(schema, function(err, result) {
        var cmd=result.Chat.split(" ");
        switch(cmd[0]) {
            case "/help":
                console.log("=== Common Commands ===");
                console.log("/help - Displays the list of commands.");
                console.log("");
                console.log("=== Player Reporting ===");
                console.log("/report [socket id] [reason for report] - Sends an instance report to the host.");
                console.log("/greport [socket id] [reason for report] - Sends a global report to the Instantiated Staff team. (Note: When sending a report please do not tell the user you are reporting them. They can leave the session which will make reporting them useless.)");
                console.log("");
                console.log("=== Profile Commands ===");
                console.log("/edit-nickname [alias] - Edits the user's nickname.");
                console.log("/toggle-pm - Toggles the ability to let you see private messages.");
                console.log("/bio [your bio here] - Sets the profile's bio for others to read.");
                console.log("/status [online,away,custom <write your own status>]")
                console.log("/check-status [socket id] - Sends the socket id a message and the socket will reply back with the status.");
                console.log("/read-bio [socket id] - Reads the user's bio.");
                console.log("");
                console.log("=== Chat Commands ===");
                console.log("/pm [socket id] [message] - Sends a message to the socket if they have private messages enabled.");
                console.log("[message] - Sends chat messages to everyone on the session.");
                break;
            case "/report":
                var socket_id=parseInt(cmd[1]);
                var reason_="";
                for(var i=2; i<cmd.length; i++) {
                    if(i==2) {reason_+=cmd[i];}else{reason_+=" "+cmd[i];}
                }
                var report=JSON.stringify({
                    msg: "report",
                    instance_guid: SESSION_ID,
                    against: socket_id,
                    reason: reason_
                });
                socket.send(report);
                break;
            case "/greport":
                var socket_id=parseInt(cmd[1]);
                var reason="";
                for(var i=2; i<cmd.length; i++) {
                    if(i==2) {reason+=cmd[i];}else{reason+=" "+cmd[i];}
                }
                var globalreport=JSON.stringify({
                    msg: "FileReport",
                    socketid: socket_id,
                    Message: reason
                });
                socket.send(globalreport);
                break;
            case "/edit-nickname":
                var nickchange="";
                for(var i=1; i<cmd.length; i++) {
                    if(i==1) {nickchange+=cmd[i];}else{nickchange+=" "+cmd[i];}
                }
                profile_["nickname"] = nickchange;
                fs.writeFileSync("client-profile.json", JSON.stringify(profile_, null, '\t'));
                break;
            case "/toggle-pm":
                if(profile_["acceptPM"]==true) {profile_["acceptPM"]=false;}else{profile_["acceptPM"]=true;}
                fs.writeFileSync("client-profile.json", JSON.stringify(profile_, null, '\t'));
                break;
            case "/bio":
                var bio="";
                for(var i=1; i<cmd.length; i++) {
                    if(i==1){bio+=cmd[i];}else{bio+=" "+cmd[i];}
                }
                profile_["bio"]=bio;
                fs.writeFileSync("client-profile.json", JSON.stringify(profile_, null, '\t'));
                break;
            case "/status":
                var s="";
                for(var i=1; i<cmd.length; i++) {
                    if(i==1) {s+=cmd[i];}else{s+=" "+cmd[i];}
                }
                profile_["status"]=s;
                fs.writeFileSync("client-profile.json", JSON.stringify(profile_, null, '\t'));
                break;
            case "/check-status":
                var socket_id=parseInt(cmd[1]);

                var checkStatus=JSON.stringify({
                    msg: "socket_to",
                    DataStream: {
                        id: "check_status"
                    },
                    SocketID: socket_id
                });
                socket.send(checkStatus);
                break;
            case "/read-bio":
                var socket_id=parseInt(cmd[1]);

                var readBio=JSON.stringify({
                    msg: "socket_to",
                    DataStream: {
                        id: "read_bio"
                    },
                    SocketID: socket_id
                });
                socket.send(readBio);
                break;
            case "/pm":
                var socket_id=parseInt(cmd[1]);
                var chatmsg="";
                for(var i=2; i<cmd.length; i++) {
                    if(i==2) {chatmsg+=cmd[i];}else{chatmsg+=" "+cmd[i];}
                }

                var pm=JSON.stringify({
                    msg: "socket_to",
                    DataStream: {
                        id: "pm_chat",
                        nickname: profile_["nickname"],
                        chat_message: chatmsg
                    },
                    SocketID: socket_id
                });
                socket.send(pm);
                console.log(profile_["nickname"]+" >> "+chatmsg);
                break;
            default:
                if(result.Chat != "") {
                    var send_chat=JSON.stringify({
                        msg: "sent",
                        instance_guid: SESSION_ID,
                        DataStream: {
                            id: "client_chat",
                            chatmsg: result.Chat,
                            nick: profile_["nickname"]
                        }
                    });
                    socket.send(send_chat);
                }
                break;
        }
        setTimeout(function() {promptChat();}, 1500);
    });
}

if(config["useTorConnection"]) {
    socket = new WebSocket("ws://servermckvakd3l5aswm2uklnvl4iahcvov7te3l66loj7rsnfvbdvqd.onion:12701", { agent: agent });
} else {
    socket=new WebSocket("wss://instantiated.xyz:12701");
}
socket.on("open", () => {
    setInterval(function() {
        if(socket.readyState == socket.OPEN) {
            socket.send(`{"msg":"ping"}`);
        }
    },30000);

    console.log("Chat Initialized! Remember to type /help for a list of commands.");
    var sendJoin=JSON.stringify({
        msg: "join_server",
        instance_guid: config["session_id"],
        passkey: config["session_password"],
        invite_code: config["invite_code"]
    });
    socket.send(sendJoin);
    promptChat();
});

socket.on("close", () => {
    console.log("The connection to the server was lost.");
    process.exit();
});

socket.on("error", () => {
    console.log("An error has occured.");
    process.exit();
});

socket.on("message", (msg) => {
    var id=JSON.parse(msg);

    switch(id["MSG"]) {
        case "filed_success":
            console.log("Successfully submitted your report to the staff team!");
            break;
        case "file_submit_failed":
            console.log("Report failed to submit:");
            console.log("- The socket id you provided is no longer available or around.");
            console.log("- The message was blank. Please enter a message before you report the user.");
            break;
        case "SocketID":
            console.log("Connection was successful! Your ID is "+id["SocketID"]);
            break;
        case "banned_from_session":
            var expiring=id["ExpiresOn"] - Math.floor(new Date().getTime()/1000.0);
            console.log("You have been banned from this session.");
            console.log("Reason: "+id["Reason"]);
            console.log("Expires: "+convertEpochToDate(id["ExpiresOn"]));
            console.log("Duration Time: "+expiring);
            setTimeout(function() {process.exit();}, 10000);
            break;
        case "not_on_whitelist":
            console.log("Not on the whitelist.");
            process.exit();
            break;
        case "session_full":
            console.log("The session you attempted to connect was full.");
            process.exit();
            break;
        case "session_locked":
            console.log("The session you tried to join was locked.");
            process.exit();
            break;
        case "invalid_password":
            console.log("In-correct password, please try again.");
            process.exit();
            break;
        case "session_not_found":
            console.log("Sorry but there are no sessions available with that id.");
            process.exit();
            break;
        case "kicked_from_session":
            console.log("You have been kicked from session!");
            console.log("Reason: "+id["Reason"]);
            setTimeout(function() {process.exit();}, 10000);
            break;
        case "ServerDisconnected":
            console.log("I'm sorry but the host's connection was closed or interrupted.");
            process.exit();
            break;
        case "connected_session":
            SESSION_ID=config["session_id"];
            console.log("Connected to "+SESSION_ID);
            break;
        case "Rx":
            var rx=id["DataStream"];

            switch(rx["id"]) {
                case "client_chat":
                    console.log("<"+rx["nick"]+" from socket "+rx["socket_id"]+">: "+rx["chatmsg"]);
                    break;
            }
            break;
        case "Secure_Rx":
            var srx=id["DataStream"];

            switch(srx["id"]) {
                case "create_connection":
                    if(srx["is_logging"]) {
                        console.log("WARNING: This host is logging your data stream and is storing those logs to their local disk. Anything you send will be logged for data moderation purposes.")
                    }
                    console.log("Launch "+srx["application"]+" to connect.");
                    console.log("Creating the connection...");
                    Protocol=srx["protocol"];
                    Port=srx["port"];
                    switch(srx["protocol"]) {
                        case "tcp":
                            console.log("Created TCP Connection (connect via "+config["listen_ipaddress"]+":"+Port+")");
                            Server = new TCP.createServer((mySocket) => {
                                Sockets.push(mySocket);
                                var connectToServer=JSON.stringify({
                                    msg: "sent",
                                    instance_guid: SESSION_ID,
                                    DataStream: {
                                        id: "client_connect"
                                    }
                                });
                                socket.send(connectToServer);
                                //Connection=mySocket;
                                console.log("Client connected!");
                                mySocket.on("data", (data) => {
                                    var clientSendsHost=JSON.stringify({
                                        msg: "sent",
                                        instance_guid: SESSION_ID,
                                        DataStream: {
                                            id: "client_sent",
                                            dataStream: data,
                                            pos: Sockets.indexOf(mySocket)
                                        }
                                    })
                                    socket.send(clientSendsHost);
                                });
                                mySocket.on("error", () => {
                                    console.log("There was a problem with the socket's connection.");
                                })
                                mySocket.on("close", () => {
                                    var connectToServer=JSON.stringify({
                                        msg: "sent",
                                        instance_guid: SESSION_ID,
                                        DataStream: {
                                            id: "client_disconnect",
                                            pos: Sockets.indexOf(mySocket)
                                        }
                                    });
                                    socket.send(connectToServer);
                                    console.log("Client disconnected!");
                                    Connection=null;
                                    Sockets.splice(Sockets.indexOf(mySocket), 1);
                                });
                            });
                            Server.listen(Port, config["listen_ipaddress"]);
                            break;
                        case "udp":
                            console.log("UDP Connection was created! (connect to "+config["listen_ipaddress"]+":"+Port+")");
                            ServerUDP = new UDP.createSocket("udp4");
                            ServerUDP.on("message", (msg,receInfo) => {
                                if(Connection != PreviousConnection) {console.log("Client connected!");}
                                Connection=receInfo.port;
                                PreviousConnection=Connection;
                                var clientSendsHost=JSON.stringify({
                                    msg: "sent",
                                    instance_guid: SESSION_ID,
                                    DataStream: {
                                        id: "client_sent",
                                        dataStream: msg
                                    }
                                });
                                socket.send(clientSendsHost);
                            });
                            ServerUDP.on("error", () => {
                                console.log("There was a problem with the socket's connection.");
                            })
                            ServerUDP.bind(Port, config["listen_ipaddress"]);
                            break;
                    }
                    break;
                case "server_sent":
                    switch(Protocol) {
                        case "tcp":
                            /* if(Connection!=null) {
                                Connection.write(Buffer.from(srx["dataStream"]));
                            } */
                            try {Sockets[srx["pos"]].write(Buffer.from(srx["dataStream"]));}catch(e){}
                            break;
                        case "udp":
                            if(Connection!=null) {
                                ServerUDP.send(Buffer.from(srx["dataStream"]), Connection, config["listen_ipaddress"]);
                            }
                            break;
                    }
                    break;
            }
            break;
        case "SocketRx":
            var s=id["DataStream"];

            switch(s["id"]) {
                case "pm_chat":
                    if(profile_["acceptPM"] == true) {
                        console.log(s["nickname"]+" from socket "+id["From"]+" << "+s["chat_message"]);
                    }
                    break;
                case "check_status":
                    var backCheckStatus=JSON.stringify({
                        msg: "socket_to",
                        DataStream: {
                            id: "got_status",
                            nickname: profile_["nickname"],
                            status: profile_["status"]
                        },
                        SocketID: id["From"]
                    });
                    socket.send(backCheckStatus);
                    break;
                case "read_bio":
                    var backBioRead=JSON.stringify({
                        msg: "socket_to",
                        DataStream: {
                            id: "got_bio",
                            nickname: profile_["nickname"],
                            bio: profile_["bio"]
                        },
                        SocketID: id["From"]
                    });
                    socket.send(backBioRead);
                    break;
                case "got_status":
                    switch(s["status"]) {
                        case "online":
                            console.log(s["nickname"]+" is Online.");
                            break;
                        case "away":
                            console.log(s["nickname"]+" is Away From Keyboard.");
                            break;
                        default:
                            console.log(s["nickname"]+" is "+s["status"]);
                            break;
                    }
                    break;
                case "got_bio":
                    console.log("["+s["nickname"]+"]\n\n"+s["bio"]);
                    break;
            }
            break;
        case "joined_session":
            console.log("Client "+id["SocketID"]+" has joined the session.");
            break;
        case "Disconnected":
            console.log("Client "+id["SocketID"]+" has lefted the session.");
            break;
    }
});