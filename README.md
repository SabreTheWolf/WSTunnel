# Introduction

WSTunnel allows for anyone to easily host a TCP/UDP server where data is sent via Websockets.

You may use the release section to download the binary version that is already compiled for you to use and configure.

# How to install

1. If you have git installed open your terminal and type in "git clone https://gitea.com/SabreTheWolf/WSTunnel". Once you have the repository cloned you should be able to cd into the project. If you do not have git installed then just download the source from the release section.
2. After you have cd'ed into the directory type in "npm install" if you have NodeJS installed if not head over to nodejs's website at https://nodejs.org/ and follow the installation instruction then retry the command again.
3. Then after you have installed the necessary packages type in "node Server.js" the application will close once for initializing the configuration file so you can modify it to your exact purposes. Do not enable useTorConnection unless you want slow speeds (using this means you are aware of the speeds. In otherwords it's just a fun little experiment for you and your friend to try and should not be used for hosting your server with strangers.)

# To be aware of...?

Becareful when enabling the useTorConnection as that requires you to run a tor socks5 connection and when hosting the session you must also be aware that IP Addresses will only be shown as 127.0.0.1 which means moderation is difficult that should not be used for hosting with strangers. If need to for whatever the reason you may want to use TOR instead of the other server then please remember to set a password for your friends only. Otherwise your gonna have a hard time moderating when you can't ban their IP Address.

Also when not using TOR's Connection to be aware that hosts will have your IP Address incase they need to ban you for whatever the reason may be. They can also track what packets you send to the server as well as logging how much data you sent if they have enabled some configuration to do so.

# Questions and Answers! (Some by me of course)

### Q: Is my session safe if I use a password so that only my friends can connect?
A: Yes it is. It uses WSS (Websocket Secure) so you aren't sending data in plaintext.
### Q: How does this work? Will my friend be able to connect to the server if they use a LAN IP like 192.168.1.122?
A: Yes they will be able to connect to your server either their Local Area Network IP or their loopback ip as long as they are connected to your session they should be able to use either 127.0.0.1 or their LAN IP.
### Q: I got kicked/banned what should I do?
A: Theres nothing you can do as it's up to the host to decide when they want to unban your ip address.